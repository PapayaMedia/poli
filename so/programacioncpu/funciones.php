<?php

function inicializar() {
  session_start();
  if ($_POST) {
    foreach ($_POST as $key => $value) {
      $_SESSION['formulario'][$key] = $value;
    }
  }
  else {
    $_SESSION['formulario']['tllegada'] = implode("\n", array(0, 3, 5, 9, 10, 12, 14, 16, 17, 19));
    $_SESSION['formulario']['tcpu'] = implode("\n", array(6, 2, 1, 7, 5, 3, 4, 5, 7, 2));
    $_SESSION['formulario']['quantum'] = 4;
  }
  revisar_formulario();
}

function formulario() {
  $output = '';
  $output .= '
    <form method="post">
      <table>
        <tr><th>Tiempos de llegada</th><th>Tiempos de CPU</th></tr>
        <tr>
          <td><textarea name="tllegada" rows="15">' . $_SESSION['datos']['tllegada'] . '</textarea></td>
          <td><textarea name="tcpu" rows="15">' . $_SESSION['datos']['tcpu'] . '</textarea></td>
        </tr>
      </table>
      Quantum: <input type="textfield" name="quantum" value="' . $_SESSION['formulario']['quantum'] . '" />
      <input type="submit" value="Calcular" />
    </form>';
  return $output;
}

function revisar_formulario() {
  if (is_array($_SESSION['formulario'])) {
    verificar_tllegada();
    verificar_tcpu();
    verificar_cantidades();
    verificar_quantum();
    $_SESSION['evaluar-algoritmos'] = FALSE;
    if (!$_SESSION['error-formulario']) {
      crear_mensaje('Los datos son correctos.');
      $_SESSION['evaluar-algoritmos'] = TRUE;
    }
  }
}

function verificar_tllegada() {
  $_SESSION['formulario']['tllegada'] = rectificar_tiempos(explode("\n", $_SESSION['formulario']['tllegada']));
  $_SESSION['datos']['tllegada'] = "";
  foreach ($_SESSION['formulario']['tllegada'] as $t) {
    $_SESSION['datos']['tllegada'] .= ($t . "\n");
  }

}

function verificar_tcpu() {
  $_SESSION['formulario']['tcpu'] = rectificar_tiempos(explode("\n", $_SESSION['formulario']['tcpu']));
  $_SESSION['datos']['tcpu'] = "";
  foreach ($_SESSION['formulario']['tcpu'] as $t) {
    $_SESSION['datos']['tcpu'] .= ($t . "\n");
  }
}

function verificar_cantidades() {
  if (count($_SESSION['formulario']['tllegada']) == 0 || (count($_SESSION['formulario']['tllegada']) != count($_SESSION['formulario']['tcpu']))) {
    crear_mensaje('Revise los tiempos de llegada y CPU.', 'error');
  }
}

function verificar_quantum() {
  $q = trim($_SESSION['formulario']['quantum']);
  if (is_numeric($q)) {
    $_SESSION['formulario']['quantum'] = (int) $_SESSION['formulario']['quantum'];
  }
  else {
    crear_mensaje('Revise el valor del Quantum.', 'error');
  }
}

function rectificar_tiempos($tiempos) {
  $tcorregido = array();
  $i = 0;
  foreach ($tiempos as $t) {
    $t = trim($t);
    if (is_numeric($t)) {
      $tcorregido[$i] = (int) $t;
      $i++;
    }
  }
  return $tcorregido;
}

function algoritmos() {
  $algoritmos = array(
    'fcfs',
    'sjn',
    'srt',
    'rr'
  );
  $_SESSION['tiempos'] = array();
  $output = '';
  foreach ($algoritmos as $algoritmo) {
    $funcion = 'algoritmo_' . $algoritmo;
    $output .= $funcion($_SESSION['formulario']['tllegada'], $_SESSION['formulario']['tcpu']);
  }
  return $output;
}

function algoritmo_fcfs($tllegada, $tcpu) {
  $output = '';
  $_SESSION['tiempos']['fcfs']['espera'] = array();
  $_SESSION['tiempos']['fcfs']['retorno'] = array();
  asort($tllegada);
  $tiempos = array();
  $temp = 0;
  $total_espera = 0;
  $total_retorno = 0;
  foreach ($tllegada as $p => $tl) {
    $inicio = max($temp, $tl);
    $fin = $inicio + $tcpu[$p];
    $temp = $fin;
    $id = $inicio . '_' . $fin;
    $tiempos[$id] = $p;
    $tiempo_espera = $inicio - $tl;
    $total_espera += $tiempo_espera;
    $_SESSION['tiempos']['fcfs']['espera'][$p] = $tiempo_espera;
    $tiempo_retorno = $fin - $tl;
    $total_retorno += $tiempo_retorno;
    $_SESSION['tiempos']['fcfs']['retorno'][$p] = $tiempo_retorno;
  }
  $_SESSION['tiempos']['fcfs']['espera']['promedio'] = $total_espera / count($tllegada);
  $_SESSION['tiempos']['fcfs']['retorno']['promedio'] = $total_retorno / count($tllegada);
  $output .= pintar_ejecucion('fcfs', $tiempos);
  return $output;
}

function algoritmo_sjn($tllegada, $tcpu) {
  $output = '';
  $_SESSION['tiempos']['sjn']['espera'] = array();
  $_SESSION['tiempos']['sjn']['retorno'] = array();
  asort($tllegada);
  $tiempos = array();
  $temp = 0;
  $total_espera = 0;
  $total_retorno = 0;
  while (count($tcpu)) {
    $p = mas_corto_disponible($temp, $tllegada, $tcpu);
    $inicio = max($temp, $tllegada[$p]);
    $fin = $inicio + $tcpu[$p];
    $temp = $fin;
    $id = $inicio . '_' . $fin;
    $tiempos[$id] = $p;
    $tiempo_espera = $inicio - $tllegada[$p];
    $total_espera += $tiempo_espera;
    $_SESSION['tiempos']['sjn']['espera'][$p] = $tiempo_espera;
    $tiempo_retorno = $fin - $tllegada[$p];
    $total_retorno += $tiempo_retorno;
    $_SESSION['tiempos']['sjn']['retorno'][$p] = $tiempo_retorno;
    unset($tcpu[$p]);
  }
  $_SESSION['tiempos']['sjn']['espera']['promedio'] = $total_espera / count($tllegada);
  $_SESSION['tiempos']['sjn']['retorno']['promedio'] = $total_retorno / count($tllegada);
  $output .= pintar_ejecucion('sjn', $tiempos);
  return $output;
}

function algoritmo_srt($tllegada, $tcpu) {
  $output = '';
  $_SESSION['tiempos']['srt']['espera'] = array();
  $_SESSION['tiempos']['srt']['retorno'] = array();
  asort($tllegada);
  $tiempos = array();
  $temp = 0;
  $total_espera = 0;
  $total_retorno = 0;
  $actual = -1;
  while (count($tcpu)) {
    $p = mas_corto_disponible($temp, $tllegada, $tcpu);
    $tcpu[$p] = $tcpu[$p] - 1;
    if ($tcpu[$p] == 0) {
      unset($tcpu[$p]);
    }
    if ($p != $actual) {
      if ($actual != -1) {
        $fin = $temp;
        $id = $inicio . '_' . $fin;
        $tiempos[$id] = $actual;
      }
      $inicio = $temp;
      $actual = $p;
    }
    if (!isset($tcpu[$p])) {
      $_SESSION['tiempos']['srt']['retorno'][$p] = $temp - $tllegada[$p];
      $_SESSION['tiempos']['srt']['espera'][$p] = $_SESSION['tiempos']['srt']['retorno'][$p] - $_SESSION['formulario']['tcpu'][$p] + 1;
    }
    $temp++;
  }
  $fin = $temp;
  $id = $inicio . '_' . $fin;
  $tiempos[$id] = $actual;
  $_SESSION['tiempos']['srt']['retorno'][$p] = $temp - $tllegada[$p];
  $_SESSION['tiempos']['srt']['espera'][$p] = $_SESSION['tiempos']['srt']['retorno'][$p] - $_SESSION['formulario']['tcpu'][$p];
  $_SESSION['tiempos']['srt']['espera']['promedio'] = array_sum($_SESSION['tiempos']['srt']['espera']) / count($tllegada);
  $_SESSION['tiempos']['srt']['retorno']['promedio'] = array_sum($_SESSION['tiempos']['srt']['retorno']) / count($tllegada);
  $output .= pintar_ejecucion('srt', $tiempos);
  return $output;
}



function algoritmo_rr($tllegada, $tcpu) {
  $output = '';
  $_SESSION['tiempos']['rr']['espera'] = array();
  $_SESSION['tiempos']['rr']['retorno'] = array();
  $quantum = $_SESSION['formulario']['quantum'];
  asort($tllegada);
  $tiempos = array();
  $temp = 0;
  $total_espera = 0;
  $total_retorno = 0;
  $actual = -1;
  while (count($tcpu)) {
    foreach ($tllegada as $p => $tl) {
      if (proceso_disponible($p, $temp, $tcpu)) {
        $inicio = max($temp, $tllegada[$p]);
        $ejecucion = min($quantum, $tcpu[$p]);
        $fin = $temp + $ejecucion;
        $temp = $fin;
        $id = $inicio . '_' . $fin;
        $tiempos[$id] = $p;
        $tcpu[$p] = $tcpu[$p] - $ejecucion;
        if ($tcpu[$p] == 0) {
          $_SESSION['tiempos']['rr']['espera'][$p] = $fin - $tl - $_SESSION['formulario']['tcpu'][$p];
          $_SESSION['tiempos']['rr']['retorno'][$p] = $temp - $tl;
          unset($tcpu[$p]);
        }
      }
    }
  }
  $_SESSION['tiempos']['rr']['espera']['promedio'] = array_sum($_SESSION['tiempos']['rr']['espera']) / count($tllegada);
  $_SESSION['tiempos']['rr']['retorno']['promedio'] = array_sum($_SESSION['tiempos']['rr']['retorno']) / count($tllegada);
  $output .= pintar_ejecucion('Round Robin', $tiempos);
  return $output;
}

function proceso_disponible($p, $temp, $tcpu) {
  return $_SESSION['formulario']['tllegada'][$p] <= $temp && isset($tcpu[$p]) && $tcpu[$p] > 0;
}

function mas_corto_disponible($temp, $tllegada, $tcpu) {
  $p = 0;
  $t = 999999;
  foreach ($tcpu as $key => $value) {
    if ($tllegada[$key] <= $temp && $value < $t) {
      $p = $key;
      $t = $tcpu[$key];
    }
  }
  return $p;
}

function pintar_ejecucion($algoritmo, $tiempos) {
  $factor = 20;
  $output = '';
  $output .= '<div class="algoritmo" style="width:' . ($factor * (array_sum($_SESSION['formulario']['tcpu']) + 2)) . 'px;"><div class="nombre-algoritmo">' . strtoupper($algoritmo) . '</div>';
  $intervalos = '';
  $tiempo = '';
  foreach ($tiempos as $key => $value) {
    $t = explode('_', $key);
    $ancho = $factor * ($t[1] - $t[0]);
    $intervalos .= '<div class="intervalo" style="width:' . $ancho . 'px; background-color:#' . calcular_color($value) . ';">' . $value . '</div>';
    $tiempo .= '<div class="tiempo' . ($t[0] ? '' : ' primero') . '" style="width:' . $ancho . 'px;">' . $t[0] . '</div>';
  }
  $tiempo .= '<div class="tiempo" style="width:20px;">' . $t[1] . '</div>';

  $output .= ($intervalos . $tiempo . '</div>');
  return $output;
}

function calcular_color($pid) {
  $min = 800000;
  $max = 14000000;
  $procesos = count($_SESSION['formulario']['tllegada']);
  return dechex((int) ($min + ($max - $min) / $procesos * ($pid + 1)));
}

function tabla() {
  $output = '';
  $output .= '<table id="resultados" border="1">';
  $output .= '<tr>';
  $output .= '<th rowspan="2">Proceso</th>';
  $output .= '<th rowspan="2">Tiempo de Llegada</th>';
  $output .= '<th rowspan="2">Tiempo de CPU</th>';
  $output .= '<th colspan="2">FCFS</th>';
  $output .= '<th colspan="2">SJN</th>';
  $output .= '<th colspan="2">SRT</th>';
  $output .= '<th colspan="2">Round Robin</th>';
  $output .= '</tr>';
  $output .= '<tr>';
  $output .= '<th>Tiempo de Espera</th>';
  $output .= '<th>Tiempo de Retorno</th>';
  $output .= '<th>Tiempo de Espera</th>';
  $output .= '<th>Tiempo de Retorno</th>';
  $output .= '<th>Tiempo de Espera</th>';
  $output .= '<th>Tiempo de Retorno</th>';
  $output .= '<th>Tiempo de Espera</th>';
  $output .= '<th>Tiempo de Retorno</th>';
  $output .= '</tr>';
  for ($i = 0; $i < count($_SESSION['formulario']['tllegada']); $i++) {
    $output .= '<tr>';
    $output .= '<td>' . $i . '</td>';
    $output .= '<td>' . $_SESSION['formulario']['tllegada'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['formulario']['tcpu'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['fcfs']['espera'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['fcfs']['retorno'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['sjn']['espera'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['sjn']['retorno'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['srt']['espera'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['srt']['retorno'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['rr']['espera'][$i] . '</td>';
    $output .= '<td>' . $_SESSION['tiempos']['rr']['retorno'][$i] . '</td>';
    $output .= '</tr>';
  }
  $output .= '<tr>';
  $output .= '<th colspan="3">Promedios</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['fcfs']['espera']['promedio'], 2) . '</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['fcfs']['retorno']['promedio'], 2) . '</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['sjn']['espera']['promedio'], 2) . '</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['sjn']['retorno']['promedio'], 2) . '</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['srt']['espera']['promedio'], 2) . '</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['srt']['retorno']['promedio'], 2) . '</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['rr']['espera']['promedio'], 2) . '</th>';
  $output .= '<th>' . number_format($_SESSION['tiempos']['rr']['retorno']['promedio'], 2) . '</th>';
  $output .= '</tr>';
  $output .= '</table>';
  return $output;
}

function crear_mensaje($texto, $tipo = 'normal') {
  $_SESSION['mensajes'][$tipo][] = $texto;
  if ($tipo == 'error') {
    $_SESSION['error-formulario'] = TRUE;
  }
}

function mensajes() {
  $output = '';
  if (is_array($_SESSION['mensajes'])) {
    foreach ($_SESSION['mensajes'] as $tipo => $mensajes) {
      $output .= '<div class="mensajes-' . $tipo . '">';
      $output .= '<ul>';
      foreach ($mensajes as $mensaje) {
        $output .= '<li>' . $mensaje . '</li>';
      }
      $output .= '</ul>';
      $output .= '</div>';
    }
  }
  unset($_SESSION['mensajes']);
  if ($output) {
    return '<div class="mensajes">' . $output . '</div>';
  }
  return $output;
}

function terminar() {
  $_SESSION['error-formulario'] = FALSE;
}

