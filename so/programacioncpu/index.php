<?php
  include './funciones.php';
  inicializar();
?>
<html>
  <header>
    <title>Algoritmos de Programaci&oacute;n de CPU</title>
    <link type="text/css" rel="stylesheet" media="all" href="./estilos.css" />
  </header>
  <body>
    <h1>Simulaci&oacute;n de Algoritmos de Programaci&oacute;n de CPU</h1>
    <h2>Juan Carlos Villegas Botero - 0722042427</h2>
    <?php
      print mensajes();
      print formulario();
      if (!$_SESSION['error-formulario']) {
        print '<hr />';
        print algoritmos();
        print '<hr />';
        print tabla();
      }
    ?>
  </body>
</html>
<?php
  terminar();
?>

