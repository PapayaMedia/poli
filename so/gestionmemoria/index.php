<?php
  include './funciones.php';
  inicializar();
?>
<html>
  <header>
    <title>Algoritmos de Gesti&oacute;n de Memoria</title>
    <link type="text/css" rel="stylesheet" media="all" href="./estilos.css" />
  </header>
  <body>
    <h1>Implementaci&oacute;n de algunos Algoritmos de Gesti&oacute;n de Memoria</h1>
    <h2>Polit&eacute;cnico Grancolombiano - Sistemas Operacionales</h2>
    <p>
      Juan Pablo Bonilla<br />
      Edwin L&oacute;pez Moreno<br />
      Juan Carlos Villegas Botero
    </p>
    <?php
      print mensajes();
      print formulario();
      if (!$_SESSION['error-formulario']) {
        print '<hr />';
        print algoritmos();
        print '<hr />';
        print tabla();
      }
    ?>
  </body>
</html>
<?php
  terminar();
?>

