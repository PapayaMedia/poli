<?php

function inicializar() {
  session_start();
  if ($_POST) {
    foreach ($_POST as $key => $value) {
      $_SESSION['formulario'][$key] = $value;
    }
  }
  else {
    $_SESSION['formulario']['tamproceso'] = implode("\n", array(5760, 4190, 3290, 2030, 2550, 6990, 8940, 740, 3930, 6890, 6580, 3820, 9140, 420, 220, 7540, 3210, 1380, 8950, 3610));
    $_SESSION['formulario']['tammemoria'] = implode("\n", array(9500, 7000, 4500, 8500, 3000, 9000, 1000, 5500, 1500, 500));
    $_SESSION['formulario']['ejecuciones'] = 50;
  }
  revisar_formulario();
}

function formulario() {
  $output = '';
  $output .= '
    <form method="post">
      <table>
        <tr><th>Tama&ntilde;os de Procesos</th><th>Bloques de memoria</th></tr>
        <tr>
          <td><textarea name="tamproceso" rows="15">' . $_SESSION['datos']['tamproceso'] . '</textarea></td>
          <td><textarea name="tammemoria" rows="15">' . $_SESSION['datos']['tammemoria'] . '</textarea></td>
        </tr>
      </table>
      Ejecuciones: <input type="textfield" name="ejecuciones" value="' . $_SESSION['formulario']['ejecuciones'] . '" />
      <input type="submit" value="Calcular" />
    </form>';
  return $output;
}

function revisar_formulario() {
  if (is_array($_SESSION['formulario'])) {
    verificar_tamproceso();
    verificar_tammemoria();
    verificar_cantidades();
    verificar_ejecuciones();
    $_SESSION['evaluar-algoritmos'] = FALSE;
    if (!$_SESSION['error-formulario']) {
      crear_mensaje('Los datos son correctos.');
      $_SESSION['evaluar-algoritmos'] = TRUE;
    }
  }
}

function verificar_tamproceso() {
  $_SESSION['formulario']['tamproceso'] = rectificar_tiempos(explode("\n", $_SESSION['formulario']['tamproceso']));
  $_SESSION['datos']['tamproceso'] = "";
  foreach ($_SESSION['formulario']['tamproceso'] as $t) {
    $_SESSION['datos']['tamproceso'] .= ($t . "\n");
  }

}

function verificar_tammemoria() {
  $_SESSION['formulario']['tammemoria'] = rectificar_tiempos(explode("\n", $_SESSION['formulario']['tammemoria']));
  $_SESSION['datos']['tammemoria'] = "";
  foreach ($_SESSION['formulario']['tammemoria'] as $t) {
    $_SESSION['datos']['tammemoria'] .= ($t . "\n");
  }
}

function verificar_cantidades() {
  if (count($_SESSION['formulario']['tamproceso']) == 0 || count($_SESSION['formulario']['tammemoria']) == 0) {
    crear_mensaje('Debe haber al menos un proceso y un bloque de memoria.', 'error');
  }
}

function verificar_ejecuciones() {
  $q = (int) trim($_SESSION['formulario']['ejecuciones']);
  if ($q > 0) {
    $_SESSION['formulario']['ejecuciones'] = $q;
  }
  else {
    crear_mensaje('Debe haber al menos una ejecuci&oacute;n de los procesos.', 'error');
  }
}

function rectificar_tiempos($tiempos) {
  $tcorregido = array();
  $i = 0;
  foreach ($tiempos as $t) {
    $t = trim($t);
    if (is_numeric($t)) {
      $tcorregido[$i] = (int) $t;
      $i++;
    }
  }
  return $tcorregido;
}

function algoritmos() {
  $output = '';
  $algoritmos = lista_algoritmos();
  determinar_orden();
  $output .= '<p><strong>Orden de ejecuci&oacute;n:</strong> ';
  foreach ($_SESSION['orden'] as $pid) {
    $output .= '<span class="orden-proceso" style="background-color:#' . calcular_color($pid) . ';">' . $pid . '</span> ';
  }
  $output .= '</p>';
  $_SESSION['resultados'] = array();
  foreach ($algoritmos as $key => $value) {
    $funcion = 'algoritmo_' . $key;
    if (function_exists($funcion)) {
      $funcion();
      $funcion(TRUE);
    }
  }
  return $output;
}

function lista_algoritmos() {
  return array(
    'aleatorio' => 'Aleatorio',
    'fifo' => 'FIFO',
    'segunda' => 'Segunda Oportunidad',
    'reloj' => 'Reloj',
  );
}

function determinar_orden() {
  $_SESSION['orden'] = array();
  $cantidad = count($_SESSION['formulario']['tamproceso']);
  for ($i = 0; $i < $_SESSION['formulario']['ejecuciones']; $i++) {
    $_SESSION['orden'][] = mt_rand(0, $cantidad - 1);
  }
}

function algoritmo_aleatorio($inverso = FALSE) {
  $fallos = 0;
  $aciertos = 0;
  $orden = $_SESSION['orden'];
  $nombre = 'normal';
  if ($inverso) {
    $orden = array_reverse($_SESSION['orden']);
    $nombre = 'inverso';
  }
  $memoria = inicializar_memoria();
  $t = 0;
  foreach ($orden as $pid) {
    $pos = buscar_proceso($pid, $memoria);
    if ($pos < 0) {
      $fallos++;
      if (almacenar($pid, $memoria, $t) === FALSE) {
        $disponibles = array();
        for ($i = 0; $i < count($memoria); $i++) {
          if ($_SESSION['formulario']['tamproceso'][$pid] <= $memoria[$i]['tam']) {
            $disponibles[] = $i;
          }
        }
        if ($disponibles) {
          $bloque = mt_rand(0, count($disponibles)-1);
          $memoria[$bloque]['proceso'] = $pid;
          $memoria[$bloque]['tiempo'] = $t;
        }
      }
    }
    else {
      $aciertos++;
    }
    $t++;
  }
  $_SESSION['resultados']['aleatorio'][$nombre]['aciertos'] = $aciertos;
  $_SESSION['resultados']['aleatorio'][$nombre]['fallos'] = $fallos;
}

function algoritmo_fifo($inverso = FALSE) {
  $fallos = 0;
  $aciertos = 0;
  $orden = $_SESSION['orden'];
  $nombre = 'normal';
  if ($inverso) {
    $orden = array_reverse($_SESSION['orden']);
    $nombre = 'inverso';
  }
  $memoria = inicializar_memoria();
  $t = 0;
  foreach ($orden as $pid) {
    $pos = buscar_proceso($pid, $memoria);
    if ($pos < 0) {
      $fallos++;
      if (almacenar($pid, $memoria, $t) === FALSE) {
        usort($memoria, 'ordenar_por_tiempo');
        $bloqueviejo = -1;
        for ($i = 0; $i < count($memoria) && $bloqueviejo < 0; $i++) {
          if ($_SESSION['formulario']['tamproceso'][$pid] <= $memoria[$i]['tam']) {
            $bloqueviejo = $i;
          }
        }
        if ($bloqueviejo >= 0) {
          $memoria[$bloqueviejo]['proceso'] = $pid;
          $memoria[$bloqueviejo]['tiempo'] = $t;
        }
      }
    }
    else {
      $aciertos++;
    }
    $t++;
  }
  $_SESSION['resultados']['fifo'][$nombre]['aciertos'] = $aciertos;
  $_SESSION['resultados']['fifo'][$nombre]['fallos'] = $fallos;
}

function algoritmo_segunda($inverso = FALSE) {
  $fallos = 0;
  $aciertos = 0;
  $orden = $_SESSION['orden'];
  $nombre = 'normal';
  if ($inverso) {
    $orden = array_reverse($_SESSION['orden']);
    $nombre = 'inverso';
  }
  $memoria = inicializar_memoria();
  $t = 0;
  foreach ($orden as $pid) {
    $pos = buscar_proceso($pid, $memoria);
    if ($pos < 0) {
      $fallos++;
      if (almacenar($pid, $memoria, $t) === FALSE) {
        usort($memoria, 'ordenar_por_tiempo');
        $bloqueviejo = -1;
        for ($i = 0; $i < count($memoria) && $bloqueviejo < 0; $i++) {
          if ($_SESSION['formulario']['tamproceso'][$pid] <= $memoria[$i]['tam']) {
            $memoria[$i]['tiempo'] = $t;
            if ($memoria[$i]['bit_referencia'] == 1) {
              $memoria[$i]['bit_referencia'] = 0;
            }
            else {
              $bloqueviejo = $i;
              $memoria[$i]['proceso'] = $pid;
            }
          }
        }
      }
    }
    else {
      $aciertos++;
      $memoria[$pos]['bit_referencia'] = 1;
    }
    $t++;
  }
  $_SESSION['resultados']['segunda'][$nombre]['aciertos'] = $aciertos;
  $_SESSION['resultados']['segunda'][$nombre]['fallos'] = $fallos;
}

function algoritmo_reloj($inverso = FALSE) {
  $fallos = 0;
  $aciertos = 0;
  $orden = $_SESSION['orden'];
  $nombre = 'normal';
  if ($inverso) {
    $orden = array_reverse($_SESSION['orden']);
    $nombre = 'inverso';
  }
  $memoria = inicializar_memoria();
  $apuntador = 0;
  $t = 0;
  foreach ($orden as $pid) {
    $pos = buscar_proceso($pid, $memoria);
    if ($pos < 0) {
      $fallos++;
      if (almacenar($pid, $memoria, $t) === FALSE) {
        usort($memoria, 'ordenar_por_tiempo');
        $bloqueviejo = -1;
        while ($bloqueviejo < 0) {
          if ($_SESSION['formulario']['tamproceso'][$pid] <= $memoria[$apuntador]['tam']) {
            if ($memoria[$apuntador]['bit_referencia'] == 1) {
              $memoria[$apuntador]['bit_referencia'] = 0;
            }
            else {
              $bloqueviejo = $apuntador;
              $memoria[$apuntador]['proceso'] = $pid;
            }
          }
          $apuntador = ($apuntador + 1) % count($memoria);
        }
      }
    }
    else {
      $aciertos++;
      $memoria[$pos]['bit_referencia'] = 1;
    }
    $t++;
  }
  $_SESSION['resultados']['reloj'][$nombre]['aciertos'] = $aciertos;
  $_SESSION['resultados']['reloj'][$nombre]['fallos'] = $fallos;
}

function ordenar_por_tiempo($a, $b) {
  if ($a['tiempo'] == $b['tiempo']) {
    return 0;
  }
  elseif ($a['tiempo'] > $b['tiempo']) {
    return 1;
  }
  else {
    return -1;
  }
}

function inicializar_memoria() {
  $memoria = array();
  foreach ($_SESSION['formulario']['tammemoria'] as $capacidad) {
    $memoria[] = array(
      'tam' => $capacidad,
      'proceso' => -1,
      'tiempo' => 0,
      'bit_referencia' => 0,
    );
  }
  return $memoria;
}

function buscar_proceso($pid, $memoria) {
  $pos = -1;
  for ($i = 0; $i < count($memoria); $i++) {
    if ($pid == $memoria[$i]['proceso']) {
      return $i;
    }
  }
  return $pos;
}

function almacenar($pid, &$memoria, $t, $bit_referencia = 0) {
  for ($i = 0; $i < count($memoria); $i++) {
    if ($memoria[$i]['proceso'] == -1 && ($_SESSION['formulario']['tamproceso'][$pid] <= $memoria[$i]['tam'])) {
      $memoria[$i]['proceso'] = $pid;
      $memoria[$i]['tiempo'] = $t;
      $memoria[$i]['bit_referencia'] = $bit_referencia;
      return $i;
    }
  }
  return FALSE;
}

function calcular_color($pid) {
  $min = 800000;
  $max = 14000000;
  $procesos = count($_SESSION['formulario']['tamproceso']);
  return dechex((int) ($min + ($max - $min) / $procesos * ($pid + 1)));
}

function tabla() {
  $output = '';
  $output .= '<table id="resultados" border="1">';
  $output .= '<tr>';
  $output .= '<th rowspan="2"></th>';
  $output .= '<th colspan="' . count(lista_algoritmos()) . '">Orden normal</th>';
  $output .= '<th colspan="' . count(lista_algoritmos()) . '">Orden inverso</th>';
  $output .= '</tr>';
  $output .= '<tr>';
  $tempoutput = '';
  foreach (lista_algoritmos() as $key => $value) {
    $tempoutput .= '<th>' . $value . '</th>';
  }
  $output .= $tempoutput . $tempoutput;
  $output .= '</tr>';

  $output .= '<tr>';
  $output .= '<th>Aciertos</th>';
  foreach (lista_algoritmos() as $key => $value) {
    $output .= '<td>' . $_SESSION['resultados'][$key]['normal']['aciertos'] . '</td>';
  }
  foreach (lista_algoritmos() as $key => $value) {
    $output .= '<td>' . $_SESSION['resultados'][$key]['inverso']['aciertos'] . '</td>';
  }
  $output .= '</tr>';

  $output .= '<tr>';
  $output .= '<th>Fallos</th>';
  foreach (lista_algoritmos() as $key => $value) {
    $output .= '<td>' . $_SESSION['resultados'][$key]['normal']['fallos'] . '</td>';
  }
  foreach (lista_algoritmos() as $key => $value) {
    $output .= '<td>' . $_SESSION['resultados'][$key]['inverso']['fallos'] . '</td>';
  }
  $output .= '</tr>';
  $output .= '</table>';
  return $output;
}

function crear_mensaje($texto, $tipo = 'normal') {
  $_SESSION['mensajes'][$tipo][] = $texto;
  if ($tipo == 'error') {
    $_SESSION['error-formulario'] = TRUE;
  }
}

function mensajes() {
  $output = '';
  if (is_array($_SESSION['mensajes'])) {
    foreach ($_SESSION['mensajes'] as $tipo => $mensajes) {
      $output .= '<div class="mensajes-' . $tipo . '">';
      $output .= '<ul>';
      foreach ($mensajes as $mensaje) {
        $output .= '<li>' . $mensaje . '</li>';
      }
      $output .= '</ul>';
      $output .= '</div>';
    }
  }
  unset($_SESSION['mensajes']);
  if ($output) {
    return '<div class="mensajes">' . $output . '</div>';
  }
  return $output;
}

function terminar() {
  $_SESSION['error-formulario'] = FALSE;
}

