<?php
  include "funciones.inc.php";
  inicializar();
?>
<html>
  <head>
    <title>Aut&oacute;matas, gram&aacute;ticas y lenguajes - Proyecto de aula</title>
    <link type="text/css" rel="stylesheet" media="all" href="estilos.css" />
  </head>
  <body>
    <?php
      print mensajes();
      print encabezado();
      print formulario_gramatica();
      if ($_SESSION['generar_tabla']) {
        generar_transiciones();
        generar_tabla();
        print '<hr>' . dibujar_tabla();
        if ($_SESSION['formulario']['cadena']) {
          print '<hr>' . analizar_palabra();
        }
      }
    ?>
  </body>
</html>
